Inloggegevens
-------------
Deze gebruikers hebben allebij gebruikersrechten en geen adminrechten.
```
+-----------+---------------------+-------------+
| Voor wie? | Gebruikersnaam      | Wachtwoord  |
+===========+=====================+=============+
| Docenten  | docent@hsleiden.nl  | Docent1234  |
+-----------+---------------------+-------------+
| Studenten | student@hsleiden.nl | Student1234 |
+-----------+---------------------+-------------+
```
