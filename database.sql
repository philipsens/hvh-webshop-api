CREATE TABLE account
(
  id        SERIAL       NOT NULL,
  firstname VARCHAR(255) NOT NULL,
  surname   VARCHAR(255) NOT NULL,
  username  VARCHAR(255) NOT NULL,
  password  VARCHAR(255) NOT NULL,
  role      VARCHAR(50)  NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE product
(
  id          SERIAL       NOT NULL,
  name        VARCHAR(255) NOT NULL,
  description TEXT         NOT NULL,
  quantity    INT     DEFAULT 0,
  custom      BOOLEAN DEFAULT FALSE,
  image       VARCHAR(255) NOT NULL,
  price       FLOAT        NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE payment
(
  id        SERIAL NOT NULL,
  amount    INT  DEFAULT 1,
  date      DATE DEFAULT now(),
  account   INT    NOT NULL,
  productId SERIAL NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (account) REFERENCES account (id),
  FOREIGN KEY (productId) REFERENCES product (id)
);

CREATE TABLE basket
(
  id        SERIAL NOT NULL,
  amount    INT DEFAULT 1,
  account   INT    NOT NULL,
  productId SERIAL NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (account) REFERENCES account (id),
  FOREIGN KEY (productId) REFERENCES product (id)
);