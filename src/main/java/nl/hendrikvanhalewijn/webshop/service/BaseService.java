package nl.hendrikvanhalewijn.webshop.service;

import com.google.inject.Inject;
import nl.hendrikvanhalewijn.webshop.model.BaseModel;
import nl.hendrikvanhalewijn.webshop.persistence.BaseDAO;

import javax.ws.rs.NotFoundException;
import java.util.List;

/**
 * The type Base service.
 *
 * @param <M> the type parameter
 * @param <D> the type parameter
 */
public class BaseService<M extends BaseModel, D extends BaseDAO<M>> {
    /**
     * The Dao.
     */
    protected D dao;

    /**
     * Instantiates a new Base service.
     *
     * @param dao the dao
     */
    @Inject
    public BaseService(D dao) {
        this.dao = dao;
    }

    /**
     * Require result m.
     *
     * @param model the model
     *
     * @return the m
     */
    public M requireResult(M model) {
        if (model == null) {
            throw new NotFoundException();
        }

        return model;
    }


    /**
     * Find by id m.
     *
     * @param id the id
     *
     * @return the m
     */
    public M findById(int id) {
        return requireResult(dao.findById(id));
    }

    /**
     * Find all list.
     *
     * @return the list
     */
    public List<M> findAll() {
        return dao.findAll();
    }

    /**
     * Create.
     *
     * @param obj the obj
     */
    public void create(M obj) {
        dao.create(obj);
    }

    /**
     * Delete.
     *
     * @param obj the obj
     */
    public void delete(M obj) {
        dao.delete(obj);
    }

    /**
     * Update.
     *
     * @param obj the obj
     * @param id  the id
     */
    public void update(M obj, int id) {
        obj.setId(id);
        dao.update(obj);
    }
}
