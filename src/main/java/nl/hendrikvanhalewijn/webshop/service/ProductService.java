package nl.hendrikvanhalewijn.webshop.service;

import nl.hendrikvanhalewijn.webshop.model.ProductModel;
import nl.hendrikvanhalewijn.webshop.persistence.ProductDAO;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;

/**
 * The type Product service.
 */
@Singleton
public class ProductService extends BaseService<ProductModel, ProductDAO> {
    /**
     * Instantiates a new Product service.
     *
     * @param dao the dao
     */
    @Inject
    public ProductService(ProductDAO dao) {
        super(dao);
    }

    /**
     * Find by description list.
     *
     * @param description the description
     *
     * @return the list
     */
    public List<ProductModel> findByDescription(String description) {
        return this.dao.findByDescription(description);
    }
}
