package nl.hendrikvanhalewijn.webshop.service;

import nl.hendrikvanhalewijn.webshop.model.AccountModel;
import nl.hendrikvanhalewijn.webshop.model.RegisterModel;
import nl.hendrikvanhalewijn.webshop.model.Role;
import nl.hendrikvanhalewijn.webshop.persistence.AccountDAO;

import javax.inject.Inject;
import java.util.Optional;

/**
 * The type Account service.
 */
public class AccountService extends BaseService<AccountModel, AccountDAO> {
    /**
     * Instantiates a new Account service.
     *
     * @param dao the dao
     */
    @Inject
    public AccountService(AccountDAO dao) {
        super(dao);
    }

    /**
     * Gets by credentials.
     *
     * @param username the username
     * @param password the password
     *
     * @return the by credentials
     */
    public Optional<AccountModel> getByCredentials(String username, String password) {
        return dao.getByCredentials(username, password);
    }

    /**
     * Register.
     *
     * @param registerModel the register model
     */
    public void register(RegisterModel registerModel) {
        AccountModel accountModel = new AccountModel();
        accountModel.setUsername(registerModel.getUsername());
        accountModel.setFirstname(registerModel.getFirstname());
        accountModel.setSurname(registerModel.getSurname());
        accountModel.setPassword(registerModel.getPassword());
        accountModel.setRole(Role.USER);
        dao.create(accountModel);
    }
}
