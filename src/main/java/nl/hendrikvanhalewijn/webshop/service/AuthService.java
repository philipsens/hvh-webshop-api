package nl.hendrikvanhalewijn.webshop.service;

import io.dropwizard.auth.AuthenticationException;
import io.dropwizard.auth.Authenticator;
import io.dropwizard.auth.Authorizer;
import io.dropwizard.auth.UnauthorizedHandler;
import io.dropwizard.auth.basic.BasicCredentials;
import io.dropwizard.hibernate.UnitOfWork;
import nl.hendrikvanhalewijn.webshop.model.AccountModel;
import nl.hendrikvanhalewijn.webshop.model.Role;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.core.Response;
import java.util.Optional;

/**
 * The type Auth service.
 */
@Singleton
public class AuthService implements Authenticator<BasicCredentials, AccountModel>, Authorizer<AccountModel>, UnauthorizedHandler {
    private AccountService service;

    /**
     * Instantiates a new Auth service.
     *
     * @param service the service
     */
    @Inject
    public AuthService(AccountService service) {
        this.service = service;
    }

    @UnitOfWork
    public Optional<AccountModel> authenticate(BasicCredentials credentials) throws AuthenticationException {
        return service.getByCredentials(credentials.getUsername(), credentials.getPassword());
    }

    @Override
    public boolean authorize(AccountModel model, String rolname) {
        return model.hasRole(Role.valueOf(rolname));
    }

    @Override
    public Response buildResponse(String prefix, String realm) {
        throw new ForbiddenException();
    }
}