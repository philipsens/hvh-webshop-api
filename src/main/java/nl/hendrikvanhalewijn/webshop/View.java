package nl.hendrikvanhalewijn.webshop;

/**
 * The type View.
 */
public class View {
    private View() {
    }

    /**
     * The interface Public.
     */
    public interface Public {
    }

    /**
     * The type Internal.
     */
    public static class Internal extends Private {
    }

    /**
     * The type Private.
     */
    public static class Private extends Protected {
    }

    /**
     * The type Protected.
     */
    public static class Protected implements Public {
    }
}
