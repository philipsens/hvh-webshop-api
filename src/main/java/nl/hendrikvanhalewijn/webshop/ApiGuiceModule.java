package nl.hendrikvanhalewijn.webshop;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import io.dropwizard.db.PooledDataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.hibernate.ScanningHibernateBundle;
import io.dropwizard.setup.Bootstrap;
import org.hibernate.SessionFactory;

/**
 * The type Api guice module.
 */
public class ApiGuiceModule extends AbstractModule {
    private final HibernateBundle<ApiConfiguration> hibernateBundle;

    /**
     * Instantiates a new Api guice module.
     *
     * @param bootstrap the bootstrap
     */
    public ApiGuiceModule(Bootstrap<ApiConfiguration> bootstrap) {

        hibernateBundle = new ScanningHibernateBundle<ApiConfiguration>("nl.hendrikvanhalewijn.webshop.model") {
            @Override
            public PooledDataSourceFactory getDataSourceFactory(ApiConfiguration configuration) {
                return configuration.getDataSourceFactory();
            }
        };

        bootstrap.addBundle(hibernateBundle);
    }

    @Override
    protected void configure() {
    }

    /**
     * Provide session factory session factory.
     *
     * @return the session factory
     */
    @Provides
    public SessionFactory provideSessionFactory() {
        return hibernateBundle.getSessionFactory();
    }

    /**
     * Gets hibernate bundle.
     *
     * @return the hibernate bundle
     */
    public HibernateBundle<ApiConfiguration> getHibernateBundle() {
        return hibernateBundle;
    }
}
