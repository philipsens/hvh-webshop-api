package nl.hendrikvanhalewijn.webshop.persistence;

/**
 * The interface Tri function.
 *
 * @param <T> the type parameter
 * @param <U> the type parameter
 * @param <V> the type parameter
 */
public interface TriFunction<T, U, V> {
    /**
     * Apply.
     *
     * @param t the t
     * @param u the u
     * @param v the v
     */
    void apply(T t, U u, V v);
}
