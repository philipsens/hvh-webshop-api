package nl.hendrikvanhalewijn.webshop.persistence;

import nl.hendrikvanhalewijn.webshop.model.BaseModel;
import org.hibernate.Session;
import org.hibernate.query.Query;

import javax.inject.Singleton;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 * The type Finder.
 *
 * @param <T> the type parameter
 * @param <D> the type parameter
 */
@Singleton
public class Finder<T extends BaseModel, D extends BaseDAO<T>> {
    private D dao;
    private Class<T> type;

    /**
     * Instantiates a new Finder.
     *
     * @param type the type
     * @param dao  the dao
     */
    public Finder(Class<T> type, D dao) {
        this.type = type;
        this.dao = dao;
    }

    /**
     * Find by r.
     *
     * @param <R>        the type parameter
     * @param buildQuery the build query
     * @param extractor  the extractor
     *
     * @return the r
     */
    public <R> R findBy(TriFunction<CriteriaBuilder, CriteriaQuery<?>, Root<?>> buildQuery, QueryExtractor<T, R> extractor) {
        Session session = this.dao.currentSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(type);
        Root<T> root = criteriaQuery.from(type);

        buildQuery.apply(criteriaBuilder, criteriaQuery, root);

        Query<T> q = session.createQuery(criteriaQuery);

        return extractor.extract(q);
    }
}