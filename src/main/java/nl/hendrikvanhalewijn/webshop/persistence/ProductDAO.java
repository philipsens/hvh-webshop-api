package nl.hendrikvanhalewijn.webshop.persistence;

import nl.hendrikvanhalewijn.webshop.model.ProductModel;
import org.hibernate.SessionFactory;

import javax.inject.Inject;
import java.util.List;

/**
 * The type Product dao.
 */
public class ProductDAO extends BaseDAO<ProductModel> {
    /**
     * The Finder.
     */
    Finder<ProductModel, ProductDAO> finder;

    /**
     * Instantiates a new Product dao.
     *
     * @param sessionFactory the session factory
     */
    @Inject
    public ProductDAO(SessionFactory sessionFactory) {
        super(ProductModel.class, sessionFactory);

        this.finder = new Finder(ProductModel.class, this);
    }

    /**
     * Find by description list.
     *
     * @param description the description
     *
     * @return the list
     */
    public List<ProductModel> findByDescription(String description) {
        return this.finder.findBy((criteriaBuilder, criteriaQuery, root) -> criteriaQuery.where(criteriaBuilder.like(root.get("description"), "%" + description + "%")), query -> query.list());
    }
}
