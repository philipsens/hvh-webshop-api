package nl.hendrikvanhalewijn.webshop.persistence;

import org.hibernate.query.Query;

/**
 * The interface Query extractor.
 *
 * @param <T> the type parameter
 * @param <R> the type parameter
 */
public interface QueryExtractor<T, R> {
    /**
     * Extract r.
     *
     * @param query the query
     *
     * @return the r
     */
    R extract(Query<T> query);
}
