package nl.hendrikvanhalewijn.webshop.persistence;

import com.google.inject.Inject;
import nl.hendrikvanhalewijn.webshop.model.AccountModel;
import org.hibernate.SessionFactory;

import javax.inject.Singleton;
import java.util.Optional;

/**
 * The type Account dao.
 */
@Singleton
public class AccountDAO extends BaseDAO<AccountModel> {
    private Finder<AccountModel, AccountDAO> finder;

    /**
     * Instantiates a new Account dao.
     *
     * @param factory the factory
     */
    @Inject
    public AccountDAO(SessionFactory factory) {
        super(AccountModel.class, factory);
        this.finder = new Finder(AccountModel.class, this);
    }

    /**
     * Gets by credentials.
     *
     * @param username the username
     * @param password the password
     *
     * @return the by credentials
     */
    public Optional<AccountModel> getByCredentials(String username, String password) {
        return this.finder.findBy((criteriaBuilder, criteriaQuery, root) -> criteriaQuery.where(criteriaBuilder.and(criteriaBuilder.equal(root.get("username"), username), criteriaBuilder.equal(root.get("password"), password))), query -> query.uniqueResultOptional());
    }

    /**
     * Sign up.
     *
     * @param accountModel the account model
     */
    public void signUp(AccountModel accountModel) {
        currentSession().save(accountModel);
    }
}