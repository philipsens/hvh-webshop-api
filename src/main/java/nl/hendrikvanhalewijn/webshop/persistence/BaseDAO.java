package nl.hendrikvanhalewijn.webshop.persistence;

import nl.hendrikvanhalewijn.webshop.model.BaseModel;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

/**
 * The type Base dao.
 *
 * @param <T> the type parameter
 */
public class BaseDAO<T extends BaseModel> {
    private final Class<T> type;
    private final SessionFactory sessionFactory;

    /**
     * Instantiates a new Base dao.
     *
     * @param type           the type
     * @param sessionFactory the session factory
     */
    public BaseDAO(Class<T> type, SessionFactory sessionFactory) {
        this.type = type;
        this.sessionFactory = sessionFactory;
    }

    /**
     * Current session session.
     *
     * @return the session
     */
    protected Session currentSession() {
        return this.sessionFactory.getCurrentSession();
    }

    /**
     * Find by id t.
     *
     * @param id the id
     *
     * @return the t
     */
    public T findById(int id) {
        return currentSession().get(type, id);
    }

    /**
     * Find all list.
     *
     * @return the list
     */
    public List<T> findAll() {
        CriteriaBuilder criteriaBuilder = currentSession().getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(type);
        criteriaQuery.from(type);
        return currentSession().createQuery(criteriaQuery).getResultList();

    }

    /**
     * Create.
     *
     * @param obj the obj
     */
    public void create(T obj) {
        currentSession().save(obj);
    }

    /**
     * Delete.
     *
     * @param obj the obj
     */
    public void delete(T obj) {
        currentSession().delete(obj);
    }

    /**
     * Update.
     *
     * @param obj the obj
     */
    public void update(T obj) {
        currentSession().update(obj);
    }
}