package nl.hendrikvanhalewijn.webshop.resource;

import com.fasterxml.jackson.annotation.JsonView;
import io.dropwizard.hibernate.UnitOfWork;
import nl.hendrikvanhalewijn.webshop.View;
import nl.hendrikvanhalewijn.webshop.model.AccountModel;
import nl.hendrikvanhalewijn.webshop.model.CredentialsModel;
import nl.hendrikvanhalewijn.webshop.model.Role;
import nl.hendrikvanhalewijn.webshop.service.AccountService;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;

/**
 * The type Login resource.
 */
@Provider
@Singleton
@Path("/login")
public class LoginResource {
    private final AccountService service;

    /**
     * Instantiates a new Login resource.
     *
     * @param service the service
     */
    @Inject
    public LoginResource(AccountService service) {
        this.service = service;
    }

    /**
     * Get account model.
     *
     * @param credentials the credentials
     *
     * @return the account model
     */
    @POST
    @UnitOfWork
    @Consumes(MediaType.APPLICATION_JSON)
    @JsonView(View.Public.class)
    @Produces(MediaType.APPLICATION_JSON)
    public AccountModel get(@Valid CredentialsModel credentials) {
        return service.getByCredentials(credentials.getUsername(), credentials.getPassword()).orElseThrow(() -> new NotAuthorizedException("Required credentials"));
    }

    /**
     * Sign up string.
     *
     * @param firstname    the firstname
     * @param surname      the surname
     * @param username     the username
     * @param accountsnaam the accountsnaam
     * @param password     the password
     *
     * @return the string
     */
    @GET
    @Path("/register")
    @UnitOfWork
    @Consumes(MediaType.APPLICATION_JSON)
    @JsonView(View.Public.class)
    public String signUp(@QueryParam("firstname") String firstname, @QueryParam("surname") String surname, @QueryParam("username") String username, String accountsnaam, @QueryParam("password") String password) {
        AccountModel accountModel = new AccountModel(accountsnaam, firstname, surname, username, Role.USER);
        service.create(accountModel);
        return username + password;
    }
}
