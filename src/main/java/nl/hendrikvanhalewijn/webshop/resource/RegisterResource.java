package nl.hendrikvanhalewijn.webshop.resource;

import com.fasterxml.jackson.annotation.JsonView;
import io.dropwizard.hibernate.UnitOfWork;
import nl.hendrikvanhalewijn.webshop.View;
import nl.hendrikvanhalewijn.webshop.model.RegisterModel;
import nl.hendrikvanhalewijn.webshop.service.AccountService;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;

/**
 * The type Register resource.
 */
@Provider
@Singleton
@Path("/register")
public class RegisterResource {
    private final AccountService service;

    /**
     * Instantiates a new Register resource.
     *
     * @param service the service
     */
    @Inject
    public RegisterResource(AccountService service) {
        this.service = service;
    }


    /**
     * Register.
     *
     * @param registerModel the register model
     */
    @POST
    @UnitOfWork
    @Consumes(MediaType.APPLICATION_JSON)
    @JsonView(View.Public.class)
    public void register(@Valid RegisterModel registerModel) {
        service.register(registerModel);
    }
}
