package nl.hendrikvanhalewijn.webshop.resource;

import com.fasterxml.jackson.annotation.JsonView;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import io.dropwizard.hibernate.UnitOfWork;
import nl.hendrikvanhalewijn.webshop.View;
import nl.hendrikvanhalewijn.webshop.model.BaseModel;
import nl.hendrikvanhalewijn.webshop.persistence.BaseDAO;
import nl.hendrikvanhalewijn.webshop.service.BaseService;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * The type Base resource.
 *
 * @param <M> the type parameter
 * @param <D> the type parameter
 * @param <S> the type parameter
 */
@Singleton
public class BaseResource<M extends BaseModel, D extends BaseDAO<M>, S extends BaseService<M, D>> {
    /**
     * The Service.
     */
    protected S service;

    /**
     * Instantiates a new Base resource.
     *
     * @param service the service
     */
    @Inject
    public BaseResource(S service) {
        this.service = service;
    }

    /**
     * Find by id m.
     *
     * @param id the id
     *
     * @return the m
     */
    @GET
    @Path("/{id}")
    @UnitOfWork
    @JsonView(View.Protected.class)
    @Produces(MediaType.APPLICATION_JSON)
    public M findById(@PathParam("id") int id) {
        return service.findById(id);
    }

    /**
     * Update.
     *
     * @param id    the id
     * @param model the model
     */
    @PUT
    @Path("/{id}")
    @UnitOfWork
    @JsonView(View.Public.class)
    @Consumes(MediaType.APPLICATION_JSON)
    public void update(@PathParam("id") int id, @Valid M model) {
        service.update(model, id);
    }

    /**
     * Delete.
     *
     * @param id the id
     */
    @DELETE
    @Path("/{id}")
    @UnitOfWork
    @JsonView(View.Public.class)
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") int id) {
        service.delete(service.findById(id));
    }

    /**
     * Create.
     *
     * @param model the model
     */
    @POST
    @UnitOfWork
    @JsonView(View.Public.class)
    @Consumes(MediaType.APPLICATION_JSON)
    public void create(@Valid M model) {
        service.create(model);
    }

    /**
     * Find all list.
     *
     * @return the list
     */
    @GET
    @UnitOfWork
    @JsonView(View.Protected.class)
    @Produces(MediaType.APPLICATION_JSON)
    public List<M> findAll() {
        return service.findAll();
    }
}
