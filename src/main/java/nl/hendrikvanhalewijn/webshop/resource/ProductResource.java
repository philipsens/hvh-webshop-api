package nl.hendrikvanhalewijn.webshop.resource;

import com.fasterxml.jackson.annotation.JsonView;
import io.dropwizard.hibernate.UnitOfWork;
import nl.hendrikvanhalewijn.webshop.View;
import nl.hendrikvanhalewijn.webshop.model.ProductModel;
import nl.hendrikvanhalewijn.webshop.persistence.ProductDAO;
import nl.hendrikvanhalewijn.webshop.service.ProductService;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * The type Product resource.
 */
@Singleton
@Path("/product")
public class ProductResource extends BaseResource<ProductModel, ProductDAO, ProductService> {

    /**
     * Instantiates a new Product resource.
     *
     * @param service the service
     */
    @Inject
    public ProductResource(ProductService service) {
        super(service);
    }

    @RolesAllowed("ADMIN")
    @Override
    @POST
    @UnitOfWork
    @JsonView(View.Public.class)
    @Consumes(MediaType.APPLICATION_JSON)
    public void create(@Valid ProductModel model) {
        service.create(model);
    }

    @RolesAllowed("ADMIN")
    @Override
    @DELETE
    @Path("/{id}")
    @UnitOfWork
    @JsonView(View.Public.class)
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("id") int id) {
        service.delete(service.findById(id));
    }

    @RolesAllowed("ADMIN")
    @PUT
    @Path("/{id}")
    @UnitOfWork
    @JsonView(View.Public.class)
    @Consumes(MediaType.APPLICATION_JSON)
    public void update(@PathParam("id") int id, @Valid ProductModel model) {
        service.update(model, id);
    }

    /**
     * Find by description list.
     *
     * @param description the description
     *
     * @return the list
     */
    @GET
    @Path("/search/")
    @UnitOfWork
    @JsonView(View.Protected.class)
    @Produces(MediaType.APPLICATION_JSON)
    public List<ProductModel> findByDescription(@QueryParam("description") String description) {
        return service.findByDescription(description);
    }

}
