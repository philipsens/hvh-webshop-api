package nl.hendrikvanhalewijn.webshop.model;

/**
 * The enum Role.
 */
public enum Role {/**
 * User role.
 */
USER(0),
    /**
     * Admin role.
     */
    ADMIN(1);

    private Integer bit;

    Role(int bit) {
        this.bit = bit;
    }

    /**
     * Gets bit.
     *
     * @return the bit
     */
    public Integer getBit() {
        return bit;
    }

    /**
     * Has role boolean.
     *
     * @param other the other
     *
     * @return the boolean
     */
    public boolean hasRole(Role other) {
        return this.getBit() >= other.getBit();
    }}
