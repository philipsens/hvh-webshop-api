package nl.hendrikvanhalewijn.webshop.model;

import com.fasterxml.jackson.annotation.JsonView;
import nl.hendrikvanhalewijn.webshop.View;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.security.auth.Subject;
import java.security.Principal;

/**
 * The type Account model.
 */
@Table(name = "account")
@Entity
public class AccountModel extends BaseModel implements Principal {
    @Column(name = "username")
    @NotEmpty
    @Email
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView(View.Public.class)
    private String username;

    @Column(name = "firstname")
    @NotEmpty
    @JsonView(View.Public.class)
    private String firstname;

    @Column(name = "surname")
    @NotEmpty
    @JsonView(View.Public.class)
    private String surname;

    @Column(name = "password")
    @NotEmpty
    @Length(min = 8)
    @JsonView(View.Internal.class)
    private String password;


    @Enumerated(EnumType.STRING)
    @Column(name = "role")
    @JsonView(View.Public.class)
    private Role role;


    /**
     * Instantiates a new Account model.
     */
    public AccountModel() {
    }

    /**
     * Instantiates a new Account model.
     *
     * @param username the username
     * @param password the password
     * @param role     the role
     */
    public AccountModel(String username, String password, Role role) {
        this.username = username;
        this.password = password;
        this.role = role;
    }

    /**
     * Instantiates a new Account model.
     *
     * @param firstname the firstname
     * @param surname   the surname
     * @param email     the email
     * @param password  the password
     * @param user      the user
     */
    public AccountModel(String firstname, String surname, String email, String password, Role user) {
        this.firstname = firstname;
        this.surname = surname;
        this.username = email;
        this.password = password;
        this.role = user;
    }

    @Override
    public String getName() {
        return firstname + ' ' + surname;
    }

    @Override
    public boolean implies(Subject subject) {
        return false;
    }

    /**
     * Has role boolean.
     *
     * @param role the role
     *
     * @return the boolean
     */
    public boolean hasRole(Role role) {
        return this.role == role;
    }

    /**
     * Gets username.
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets username.
     *
     * @param username the username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Gets firstname.
     *
     * @return the firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * Sets firstname.
     *
     * @param firstname the firstname
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * Gets surname.
     *
     * @return the surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Sets surname.
     *
     * @param surname the surname
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * Gets password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets password.
     *
     * @param password the password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets role.
     *
     * @return the role
     */
    public Role getRole() {
        return role;
    }

    /**
     * Sets role.
     *
     * @param role the role
     */
    public void setRole(Role role) {
        this.role = role;
    }
}