package nl.hendrikvanhalewijn.webshop.model;

import com.fasterxml.jackson.annotation.JsonView;
import nl.hendrikvanhalewijn.webshop.View;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * The type Register model.
 */
public class RegisterModel {
    @NotEmpty
    @JsonView(View.Public.class)
    private String username;

    @NotEmpty
    @JsonView(View.Public.class)
    private String firstname;

    @NotEmpty
    @JsonView(View.Public.class)
    private String surname;

    @NotEmpty
    @JsonView(View.Internal.class)
    private String password;

    /**
     * Instantiates a new Register model.
     */
    public RegisterModel() {
    }

    /**
     * Gets username.
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets username.
     *
     * @param username the username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Gets firstname.
     *
     * @return the firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * Sets firstname.
     *
     * @param firstname the firstname
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * Gets surname.
     *
     * @return the surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Sets surname.
     *
     * @param surname the surname
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * Gets password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets password.
     *
     * @param password the password
     */
    public void setPassword(String password) {
        this.password = password;
    }
}
